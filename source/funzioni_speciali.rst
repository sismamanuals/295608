.. Definizione delle immagini inline

.. |icona_up| image:: _static/icona_up.png
      :height: 1 cm
      :align: middle

.. |icona_down| image:: _static/icona_down.png
      :height: 1 cm
      :align: middle

.. |icona_funzioni_speciali_stato_macchina| image:: _static/icona_funzioni_speciali_stato_macchina.png
      :height: 1 cm
      :align: middle

.. |icona_funzioni_speciali_ultimi_allarmi| image:: _static/icona_funzioni_speciali_ultimi_allarmi.png
      :height: 1 cm
      :align: middle

.. |icona_funzioni_speciali_test_macchina| image:: _static/icona_funzioni_speciali_test_macchina.png
      :height: 1 cm
      :align: middle

.. |icona_funzioni_speciali_parametri_sistema| image:: _static/icona_funzioni_speciali_parametri_utente.png
      :height: 1 cm
      :align: middle

.. |icona_funzioni_speciali_assistenza_remota| image:: _static/icona_funzioni_speciali_assistenza_remota.png
      :height: 1 cm
      :align: middle

.. |icona_funzioni_speciali_ip_settings| image:: _static/icona_funzioni_speciali_ip_settings.png
      :height: 1 cm
      :align: middle

.. |icona_forward_2| image:: _static/icona_forward.png
      :height: 1 cm
      :align: middle

.. |icona_menù_parametri_sistema_utente_buzzer| image:: _static/icona_menù_parametri_sistema_utente_buzzer.png
      :height: 1 cm
      :align: middle

.. |icona_salva_2| image:: _static/icona_salva.png
      :height: 1 cm
      :align: middle

.. _funzioni_speciali:

#################
FUNZIONI SPECIALI
#################
Dalla pagina :guilabel:`Menù Principale` (:numref:`menù_principale_no_lock`), tramite il pulsante :kbd:`Funzioni Speciali` si accede alla pagina :guilabel:`Funzioni Speciali`.

.. _menù_funzioni_speciali:
.. figure:: _static/menù_funzioni_speciali.png
   :width: 14 cm
   :align: center

   Pagina 1 :guilabel:`Funzioni Speciali`

.. _menù_funzioni_speciali_2:
.. figure:: _static/menù_funzioni_speciali_2.png
   :width: 14 cm
   :align: center

   Pagina 2 :guilabel:`Funzioni Speciali`

Per navigare tra le schermate utilizzare i pulsanti |icona_up| e |icona_down|.

Si possono selezionare:

* |icona_funzioni_speciali_stato_macchina| per accedere a :guilabel:`Stato Macchina` (vedere :numref:`stato_macchina`);
* |icona_funzioni_speciali_ultimi_allarmi| per accedere a :guilabel:`Ultimi Allarmi` (vedere :numref:`ultimi_allarmi`);
* |icona_funzioni_speciali_test_macchina| è una funzione dedicata a personale abilitato (vedere :numref:`test_macchina`);
* |icona_funzioni_speciali_parametri_sistema| per accedere a :guilabel:`Parametri Utente` (vedere :numref:`parametri_di_sistema`);
* |icona_funzioni_speciali_ip_settings| per accedere a :guilabel:`IP Settings` (vedere :numref:`impostazioni_rete_vcn`);
* |icona_funzioni_speciali_assistenza_remota| per accedere a :guilabel:`Assistenza Remota` (vedere :numref:`assistenza_remota`).

.. _stato_macchina:

**************
STATO MACCHINA
**************
Premendo il pulsante |icona_funzioni_speciali_stato_macchina| si accede ad una serie di schermate che visualizzano i parametri di funzionamento del dispositivo.

Per spostarsi tra queste schermate è sufficiente usare le frecce di navigazione.
In successione vengono visualizzate le seguenti informazioni:

* Videata contatori:

   * numero di ore a macchina accesa;
   * numero di ore di lavoro della pompa del circuito di raffreddamento;
   * tempo mancante alla successiva manutenzione;
   * numero di impulsi effettuati dalla macchina;
   * numero di impulsi effettuati dalla lampada;
   * contatore di energia sulla lampada;
   * contatore di accensioni macchina;

.. _menù_stato_macchina_contatori:
.. figure:: _static/menù_stato_macchina_contatori.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Stato Macchina - Contatori`

Il contatore di energia sulla lampada memorizza la quantità di energia transitata. Il contatore è visibile nella pagina di stato ed è espresso in unità di misura interna (non corrispondente a Joule). Se il contatore di energia supera il valore di 2000000000 (2 miliardi) è obbligatorio sostituire la lampada. Per l'azzeramento del contatore e per le funzioni del pulsante :kbd:`Manutenzione` vedere la :numref:`manutenzione`.

* Videata valori elettrici:

   * tensione lampada;
   * tensione condensatori;
   * valore della corrente sulla lampada all'ultimo campionamento effettuato;

.. _menù_stato_macchina_valori_elettrici:
.. figure:: _static/menù_stato_macchina_valori_elettrici.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Stato Macchina - Valori elettrici`

Toccando sul touch screen in corrispondenza dei grafici visualizzati nella :numref:`menù_stato_macchina_valori_elettrici` è possibile accedere ad un'altra finestra nella quale gli stessi grafici vengono visualizzati con più dettaglio e dove è possibile anche scorrere all'indietro la finestra temporale visualizzata e quindi poter vedere gli andamenti storici dei parametri visualizzati.

* Videata temperature:

   * temperature sulle schede elettroniche;
   * temperatura acqua di raffreddamento;
   * flusso d'acqua nel circuito di raffreddamento;
   * velocità di rotazione delle ventole di raffreddamento;

.. _menù_stato_macchina_temperatura:
.. figure:: _static/menù_stato_macchina_temperatura.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Stato Macchina - Temperatura`

* Videata versioni:

   * versioni firmware e software installate.

.. _menù_stato_macchina_versioni:
.. figure:: _static/menù_stato_macchina_versioni.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Stato Macchina - Versioni`

Le videate successive contengono informazioni utili per personale esperto e formato.

.. _ultimi_allarmi:

**************
ULTIMI ALLARMI
**************
Nella pagina in :numref:`menù_stato_allarmi` vengono visualizzati gli ultimi allarmi occorsi nella giornata corrente.

È possibile, selezionando una data diversa, vedere gli allarmi occorsi anche nei giorni precedenti. La lista riporta, per ogni allarme:

1. numero progressivo dell'allarme;
#. ora in cui è stato generato l'allarme;
#. descrizione dell'allarme.

Per scorrere la lista utilizzare l'apposita barra di scorrimento laterale.

Nella pagina successiva vengono riportati eventuali dati aggiuntivi relativi allo stato della macchina al momento della generazione di un dato allarme.

.. _menù_stato_allarmi:
.. figure:: _static/menù_stato_allarmi.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Ultimi Allarmi`

Accedendo alla pagina successiva (:numref:`menù_stato_allarmi_backup`) è possibile eseguire il backup dei dati su memoria USB.

.. _menù_stato_allarmi_backup:
.. figure:: _static/menù_stato_allarmi_backup.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Ultimi Allarmi: Backup`

.. _test_macchina:

*************
TEST MACCHINA
*************
L'accesso a questi test prevede l'inserimento di una password ed è riservato solamente a Personale Tecnico Autorizzato dal Fabbricante.

.. _parametri_di_sistema:

********************
PARAMETRI DI SISTEMA
********************
Nella pagina in :numref:`menù_parametri_sistema` si può selezionare la lingua del software di gestione del touch screen. Le varie lingue disponibili sono rappresentate con le bandiere del paese corrispondente. Per attivare l'uso di una lingua premere il pulsante relativo alla lingua desiderata.

.. _menù_parametri_sistema:
.. figure:: _static/menù_parametri_sistema.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Parametri di Sistema`

Nella stessa pagina, in alto a destra, sono visualizzate data e ora impostate. Toccando in corrispondenza di questi valori si visualizza un tastierino che permette di correggere l'impostazione dell'orologio di sistema.

La seconda pagina, a cui si accede con |icona_forward_2| è la pagina :guilabel:`Parametri Utente`.

Questi parametri sono liberamente modificabili dall'utente:

* :kbd:`Parametro 1`: tempo post aspiratore fumi. Tempo (in decimi di secondo) di funzionamento dell'aspiratore dopo la fine di emissione della radiazione laser;
* :kbd:`Parametro 2`: tempo post gas di protezione (Argon). Tempo (in decimi di secondo) del flusso di gas di protezione dopo la fine di emissione della radiazione laser;
* :kbd:`Parametro 3`: tempo pre-gas di protezione (Argon). Tempo (in decimi di secondo) precedente l'inizio dell'emissione della radiazione laser durante il quale il gas inizia uscire dall'ugello;
* :kbd:`Parametro 5`: tempo di inattività della macchina prima di entrare in *ECO Sleep mode* (vedere :numref:`funzione_eco`);
* |icona_menù_parametri_sistema_utente_buzzer| Attiva/disattiva il buzzer del pannello operatore.

.. _menù_parametri_sistema_utente:
.. figure:: _static/menù_parametri_sistema_utente.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Parametri Utente` - 1

Premendo il pulsante |icona_forward_2| si visualizza una ulteriore schermata per l'associazione dei segnali del connettore del pedale.

* :kbd:`I_est1`:

   * Nessuna funzione; *oppure*
   * Attivazione elettrovalvola gas (se attivato i parametri 2 e 3 della :numref:`menù_parametri_sistema_utente` vengono ignorati);

* :kbd:`I_est3`:

   * Nessuna funzione; *oppure*
   * Attivazione elettrovalvola aria;

* :kbd:`I_1.0`:

   * Nessuna funzione; *oppure*
   * Attivazione funzione di caricamento della ricetta successiva tra quelle definite nella lista (vedere :numref:`impostazione_lista_di_ricette`)

.. _menù_parametri_sistema_utente_2:
.. figure:: _static/menù_parametri_sistema_utente_2.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Parametri Utente` - 2

Tutte le modifiche ai parametri utente andranno perse quando viene spenta la macchina. Per rendere permanenti le impostazioni selezionate è necessario salvarle tramite il pulsante |icona_salva_2|.

Premendo il pulsante di navigazione per andare alla pagina successiva si visualizza la schermata di login tramite password.

.. _menù_login_password:
.. figure:: _static/menù_login_password.png
   :width: 14 cm
   :align: center

   Pagina di Login con Password

.. CAUTION::
   |caution| Le pagine successive permettono di accedere ad una sezione del software riservata ai parametri di funzionamento della macchina. Tali parametri sono protetti da password poiché riservati solamente a Personale Tecnico Autorizzato dal Fabbricante. L'impostazione errata di questi parametri può causare malfunzionamenti e/o danni permanenti a cose e persone.

.. _impostazioni_rete_vcn:

**************************
IMPOSTAZIONI DI RETE E VNC
**************************
Questa pagina permette di impostare i parametri di rete:

* :kbd:`Nome HMI`: Visualizzazione Nome Macchina;
* Impostazioni automatiche di rete (solo per reti con DHCP);
* :kbd:`IP HMI`;
* :kbd:`Subnet Mask`;
* :kbd:`Gateway`;
* :kbd:`DNS`;
* Impostazione della configurazione di default;
* Pulsante refresh dei dati visualizzati.

La sezione permette di attivare il server VNC presente nella macchina per abilitare funzioni di desktop remoto.

.. NOTE::
   |notice| Nei modelli *READY* non è disponibile la configurazione del server VNC perché la funzione di assistenza remota non è disponibile.

L'accensione della spia indica la presenza di un collegamento di un client remoto.

.. _menù_impostazioni_indirizzo_IP:
.. figure:: _static/menù_impostazioni_indirizzo_IP.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Impostazioni Indirizzo IP`

.. _assistenza_remota:

*****************
ASSISTENZA REMOTA
*****************

.. _menù_remote_service:
.. figure:: _static/menù_remote_service.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Servizio Remoto`

Da questo menù è possibile configurare le impostazioni di assistenza remota.

Per abilitare e configurare la funzione contattare il Servizio Clienti SISMA.

.. NOTE::
   |notice| Nei modelli *READY* la funzione di assistenza remota non è disponibile.
