.. LM-D documentation master file, created by
   sphinx-quickstart on Tue Jan 7 13:21:25 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#####################
Manuale Software LM-D
#####################

.. toctree::
   :maxdepth: 3
   :numbered:

   pittogrammi
   scopo
   pagina_di_lavoro
   gestione_luci
   segnalazione_allarmi
   parametri_di_lavoro
   funzione_eco
   funzione_fps
   gestione_ricette
   menù_principale
   funzioni_speciali
   uso_mandrino
   manutenzione
   note
