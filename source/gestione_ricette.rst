.. Definizione delle immagini inline

.. |icona_menù_funzione_ricetta_salva| image:: _static/icona_salva.png
      :height: 1 cm
      :align: middle

.. |icona_menù_funzione_ricetta_apri| image:: _static/icona_menù_funzione_ricetta_apri.png
      :height: 1 cm
      :align: middle

.. |icona_forward_ricette| image:: _static/icona_forward.png
      :height: 1 cm
      :align: middle

.. |icona_menù_funzione_ricetta_export| image:: _static/icona_menù_funzione_ricetta_export.png
      :height: 1 cm
      :align: middle

.. |icona_menù_funzione_ricetta_restore| image:: _static/icona_menù_funzione_ricetta_restore.png
      :height: 1 cm
      :align: middle

.. _gestione_ricette:

################
GESTIONE RICETTE
################
Una ricetta (o programma di lavoro) è l'insieme delle impostazioni dei parametri di funzionamento del sistema.

L'utente può intervenire modificando i seguenti parametri:

* Potenza dell'impulso;
* Durata dell'impulso;
* Frequenza di ripetizione;
* Diametro della lavorazione;
* Forma d'onda dell'impulso;
* Illuminazione della camera di lavoro;
* Impostazione della modalità FPS - *Opzionale*;
* Parametri di lavoro con Mandrino - *Opzionale*.

Una ricetta è identificata da un indice e una descrizione, e raggruppa un set di impostazione di parametri di lavoro personalizzabile dall'utente.

È possibile memorizzare fino a 250 ricette.

L'utente può accedere alla pagina :guilabel:`Ricette` dalla schermata di lavoro cliccando sul tasto :kbd:`Ricette` (``I`` della :numref:`tabella_comandi`) oppure dal menù principale (:numref:`menù_principale_no_lock`).

.. _menù_funzione_ricetta:
.. figure:: _static/menù_funzione_ricetta.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Ricette`

Creare una ricetta significa definire i valori relativi ai parametri di lavoro e salvare le impostazioni del sistema, identificando il setup con un numero ed un nome.

* Per caricare una ricetta (:numref:`menù_funzione_ricetta`):

   * scorrere la lista delle ricette cliccando sulle apposite frecce verso l'alto e verso il basso o trascinando l'apposito cursore della barra di scorrimento verticale;
   * premere in corrispondenza della riga che identifica la ricetta desiderata. È possibile anche cliccare sul campo :kbd:`Indice` per accedere al tastierino numerico ed inserire direttamente il numero della ricetta;
   * premere sul pulsante |icona_menù_funzione_ricetta_apri|. Il sistema carica i parametri della ricetta;

* Per salvare una ricetta:

   * premere sul pulsante |icona_menù_funzione_ricetta_salva| per salvare i parametri della ricetta corrente. In fase di salvataggio, è possibile specificare un indice diverso da quello proposto,  per poter salvare la ricetta corrente in un'altra (equivale a *Salva come...*), potendone modificare anche la descrizione. In caso non si fornisca un indice differente la ricetta viene sovrascritta.

***********************
MODIFICA DI UNA RICETTA
***********************
Se viene modificato un parametro di lavoro la ricetta attualmente in uso viene contrassegnata con il simbolo :kbd:`*` vicino al numero della ricetta (:numref:`menù_funzione_ricetta_modifica`). Il simbolo :kbd:`*` indica che la ricetta corrente ha subito modifiche non ancora salvate.

La ricetta ``0`` viene caricata automaticamente all'avvio dell'apparecchiatura.
I parametri contenuti in questa ricetta sono quelli presenti al momento dell'ultimo spegnimento. La ricetta ``0`` non può essere sovrascritta dall'operatore.

.. _menù_funzione_ricetta_modifica:
.. figure:: _static/menù_funzione_ricetta_modifica.png
   :width: 14 cm
   :align: center

   Funzione ricette - Modifica

Ad ogni ricetta può essere associata una descrizione (esempio: "saldatura argento", "saldatura acciaio", ecc...) che può essere aggiunta o modificata in fase di salvataggio.

Toccando sul numero o sulla descrizione di un programma modificato (che presenta quindi l'asterisco vicino alla descrizione) un'apposita pagina di avviso indica che la ricetta corrente è stata modificata e viene chiesto se si vuole salvare le modifiche.

.. _menù_funzione_ricetta_salva:
.. figure:: _static/menù_funzione_ricetta_salva.png
   :width: 14 cm
   :align: center

   Funzione ricette - Richiesta di salvataggio

Se si decide di salvare le modifiche, una pagina ricorda il numero e la descrizione della ricetta modificata (:numref:`menù_funzione_ricetta_salvataggio`).
È possibile personalizzare la descrizione (per esempio: “saldatura argento”, “saldatura acciaio”, ecc.) ma anche il numero della ricetta, per non modificare la ricetta attualmente caricata. Il pulsante |icona_menù_funzione_ricetta_salva| salva i parametri.

.. _menù_funzione_ricetta_salvataggio:
.. figure:: _static/menù_funzione_ricetta_salvataggio.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Salvataggio Ricetta`

.. _impostazione_lista_di_ricette:

*****************************
IMPOSTAZIONE LISTA DI RICETTE
*****************************
.. NOTE::
   |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette.

La videata successiva a quella della lista delle Ricette è accessibile premendo il pulsante |icona_forward_ricette| della :numref:`menù_funzione_ricetta`.

.. _menù_funzione_ricetta_backup:
.. figure:: _static/menù_funzione_ricetta_backup.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Ricette: Lista Utente`

La schermata in :numref:`menù_funzione_ricetta_backup` si abilita tramite impostazione presente in :guilabel:`Parametri di Sistema` (vedere :numref:`parametri_di_sistema`).

Tale funzione imposta una lista di ricette (fino ad un massimo di 5), selezionabili tra tutte quelle disponibili.

L'operatore può selezionare ciclicamente le ricette all'interno della lista tramite attivazione dell'apposito ingresso, così come definito nella :numref:`parametri_di_sistema`.

.. _backup_e_ripristino_ricette:

*********************************
BACKUP E RIPRISTINO DELLE RICETTE
*********************************
La schermata successiva permette, per mezzo di memoria USB, di:

* esportare la ricetta corrente;
* eseguire backup di tutte le ricette;
* eseguire ripristino di tutte le ricette.

Tramite le funzionalità di backup e ripristino si può creare un set di ricette su un'apparecchiatura, replicandolo poi in diverse altre.

.. _menù_funzione_ricetta_backup_2:
.. figure:: _static/menù_funzione_ricetta_backup_2.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Ricette: Backup & Ripristino`

ESPORTARE RICETTA CORRENTE
==========================
Per eseguire l'esportazione della ricetta corrente dell'apparecchiatura:

* premere il pulsante |icona_menù_funzione_ricetta_export| della :numref:`menù_funzione_ricetta_backup_2`. Si accede alla videata che richiede di collegare alla porta USB dell'apparecchiatura una memoria USB con almeno 10 MB liberi;
* premere il pulsante :kbd:`Export` in :numref:`menù_funzione_ricetta_backup_3` ed attendere la fine delle operazioni di scrittura sulla memoria USB. Al termine è possibile rimuovere la memoria USB.

.. _menù_funzione_ricetta_backup_3:
.. figure:: _static/menù_funzione_ricetta_backup_3.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Esporta Ricetta Corrente`

L'operazione permette di ottenere un file contenente le informazioni relative alle impostazioni del sistema in formato di testo.

.. NOTE::
   |notice| Il sistema crea nella memoria USB un file denominato *em1.emi*. Il file può essere letto da un qualunque editor di testo dopo aver rinominato il file in *em1.txt*.

BACKUP RICETTE
==============
Per eseguire il backup delle ricette di un'apparecchiatura:

* premere il pulsante |icona_menù_funzione_ricetta_export| della :numref:`menù_funzione_ricetta_backup_2`. Si accede alla videata che richiede di collegare alla porta USB dell'apparecchiatura una memoria USB con almeno 10 MB liberi;
* premere il pulsante :kbd:`Backup` in :numref:`menù_funzione_ricetta_backup_4` ed attendere la fine delle operazioni di scrittura sulla memoria USB. Al termine è possibile rimuovere la memoria USB.

.. _menù_funzione_ricetta_backup_4:
.. figure:: _static/menù_funzione_ricetta_backup_4.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Backup Ricette`

RIPRISTINO RICETTE
==================
Per eseguire il ripristino di ricette o per clonare le ricette da un sistema ad un altro:

* premere il pulsante |icona_menù_funzione_ricetta_restore| della :numref:`menù_funzione_ricetta_backup_2`. Si accede alla videata che richiede di collegare alla porta USB dell'apparecchiatura una memoria USB con i file da ripristinare;
* premere il pulsante :kbd:`Restore` in :numref:`menù_funzione_ricetta_backup_5` ed attendere la fine delle operazioni di lettura dalla memoria USB. Al termine è possibile rimuovere la memoria USB dall'apparecchiatura.

.. _menù_funzione_ricetta_backup_5:
.. figure:: _static/menù_funzione_ricetta_backup_5.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Ripristino Ricette`

.. NOTE::
   |notice| Quando si esegue un backup, il sistema crea nella memoria USB un file denominato *em0.emi*. Per poter eseguire il recupero delle ricette da una memoria USB, il file precedentemente generato con il backup deve trovarsi sempre sulla cartella radice della memoria USB e con lo stesso nome. In caso contrario non sarà possibile effettuare il ripristino delle ricette.

***************
UTILITY RICETTE
***************
.. NOTE::
   |notice| Un uso scorretto delle funzioni presenti nella pagina può provocare la perdita delle ricette salvate in precedenza.

   Si consiglia l'uso solo da parte di personale esperto e formato.

La pagina :guilabel:`Utility Ricette` permette di gestire le aree di memorizzazione delle ricette, cancellandole e reimpostandole secondo le necessità tramite i pulsanti di comando visualizzati in :numref:`menù_funzione_ricetta_utility` 

.. _menù_funzione_ricetta_utility:
.. figure:: _static/menù_funzione_ricetta_utility.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Utility Ricette`
