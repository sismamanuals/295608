.. Comando di interruzione linea

.. |br| raw:: html

   <br>

.. Definizione delle immagini inline

.. |icona_modo_eco_performance| image:: _static/icona_modo_eco_performance.png
      :height: 1 cm
      :align: middle

.. |icona_modo_eco_silent| image:: _static/icona_modo_eco_silent.png
      :height: 1 cm
      :align: middle

.. |icona_tempo_sleep_mode| image:: _static/icona_tempo_sleep_mode.png
      :height: 1 cm
      :align: middle

.. |icona_salva| image:: _static/icona_salva.png
      :height: 1 cm
      :align: middle

.. |icona_eco_functions_2| image:: _static/icona_eco_functions.png
      :height: 1 cm
      :align: middle

.. _funzione_eco:

############
FUNZIONE ECO
############
Il software di gestione del dispositivo include alcune funzionalità di risparmio energia a cui si accede tramite il pulsante |icona_eco_functions_2| (``I`` della :numref:`tabella_comandi`).

.. NOTE::
   |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette.

.. _menù_funzione_eco:
.. figure:: _static/menù_funzione_eco.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`ECO`

.. _tabella_impostazioni_ECO:

.. csv-table:: Impulsi laser
   :header: "", "ICONA", "DESCRIZIONE"
   :widths: 25, 25, 45
   :align: center

   "A", "|icona_modo_eco_performance|", "Impostare il funzionamento in |br| :kbd:`Performance`"
   "B", "|icona_modo_eco_silent|", "Impostare il funzionamento in |br| :kbd:`Silent Mode`"
   "C", "|icona_tempo_sleep_mode|", "Tempo di :kbd:`ECO Sleep Mode` per lo |br| Stand-by della macchina"
   "D", "|icona_salva|", "Pulsante Salva"

* Il pulsante :kbd:`Performance` seleziona la disponibilità costante delle massime prestazioni della macchina;
* il pulsante :kbd:`Silent Mode` seleziona la modalità di funzionamento silenziosa (con ridotta velocità delle ventole di raffreddamento).
* La casella :kbd:`ECO Sleep Mode` definisce il tempo di inattività della macchina dopo il quale si attiva la modalità di stand-by. Se impostato a 0 la modalità è disattivata.

.. NOTE::
   |notice| La modalità Stand-by riduce i consumi e, poiché vengono spente le parti più importanti e delicate del sistema, ne allunga il tempo di servizio.

.. NOTE::
   |notice| Nel caso sia presente la funzione *BOOST* (:numref:`funzione_boost`) la modalità di funzionamento silenziosa non sarà attivabile.

Una volta impostati i valori premere il pulsante |icona_salva| per rendere le modifiche definitive.
