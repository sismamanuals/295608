.. Comando di interruzione linea

.. |br| raw:: html

    <br>

.. Definizione delle immagini inline

.. |icona_menù_principale_special_functions| image:: _static/icona_menù_principale_special_functions.png
      :height: 1 cm
      :align: middle

.. |icona_menù_principale_ricette| image:: _static/icona_menù_principale_ricette.png
      :height: 1 cm
      :align: middle

.. |icona_menù_pagina_lavoro| image:: _static/icona_menù_pagina_lavoro.png
      :height: 1 cm
      :align: middle

.. |icona_finestra_allarmi_2| image:: _static/icona_finestra_allarmi.png
      :height: 1 cm
      :align: middle

.. |icona_menù_principale_locked| image:: _static/icona_menù_principale_locked.png
      :height: 1 cm
      :align: middle

.. |icona_menù_principale_unlocked| image:: _static/icona_menù_principale_unlocked.png
      :height: 1 cm
      :align: middle

.. _menù_principale:

###############
MENÙ PRINCIPALE
###############
La pagina :guilabel:`Menù Principale` (:numref:`menù_principale_no_lock`) permette di selezionare:

* |icona_menù_principale_special_functions| per accedere a :guilabel:`Funzioni Speciali` (vedere :numref:`funzioni_speciali`);
* |icona_menù_principale_ricette| per accedere a :guilabel:`Ricette` (vedere :numref:`gestione_ricette`);
* |icona_menù_pagina_lavoro| per accedere a :guilabel:`Pagina di Lavoro` (vedere :numref:`pagina_di_lavoro`);
* |icona_finestra_allarmi_2| per accedere a :guilabel:`Allarmi` (vedere :numref:`segnalazione_allarmi`).

.. _menù_principale_no_lock:
.. figure:: _static/menù_principale_no_lock.png
   :width: 14 cm
   :align: center

   :guilabel:`Menù Principale`

***********************************
BLOCCO MODIFICA PARAMETRI DI LAVORO
***********************************
.. NOTE::
   |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette.

Questa funzione impedisce la modifica dei parametri di lavoro da parte di personale non abilitato.

Solamente il caricamento di *ricette* è permesso.

Se la funzione è abilitata appare l'icona |icona_menù_principale_locked| nella pagina del :guilabel:`Menù Principale` (:numref:`menù_principale_locked`).

.. _menù_principale_locked:
.. figure:: _static/menù_principale_locked.png
   :width: 14 cm
   :align: center

   :guilabel:`Menù Principale` con icona di abilitazione funzione

•	|icona_menù_principale_locked| indica che è attiva la funzione del sistema di protezione. La modifica dei parametri di lavoro NON è permessa.
•	|icona_menù_principale_unlocked| indica che è attiva la funzione del sistema di protezione. La modifica dei parametri di lavoro è permessa perché l'utente si è loggato ed è stato riconosciuto come utente abilitato.

***************************
GESTIONE FUNZIONE DI BLOCCO
***************************
Premendo su |icona_menù_principale_locked|, presente nella pagina :guilabel:`Menù Principale`, è possibile accedere alla pagina (:numref:`menù_principale_login`) per:

* effettuare il login come utente abilitato;
* effettuare il logout per ripristinare il blocco delle modifiche;
* cambiare la password.

.. _menù_principale_login:
.. figure:: _static/menù_principale_login.png
   :width: 14 cm
   :align: center

   Pagina di gestione funzione di blocco

CAMBIO PASSWORD
===============
Al primo utilizzo è necessario modificare la password per l'abilitazione della funzione di blocco.

La password di fabbrica è: ``1234``.

.. NOTE::
   |notice| La password non è ripristinabile dall'utente. In caso di smarrimento chiamare il Servizio Clienti SISMA.

Per cambiare la password:

* premere sul pulsante :kbd:`Cambia Password` presente nella pagina :guilabel:`Menù Principale`;
* nella pagina in :numref:`menù_principale_cambio_password` inserire la password attuale;
* se la password è corretta si abilitano le caselle di inserimento nuova password (:numref:`menù_principale_cambio_password_2`);
* inserire la nuova password nelle due caselle;
* se le password sono identiche appare il pulsante :kbd:`Cambia Password` (:numref:`menù_principale_cambio_password_3`);
* premere il pulsante :kbd:`Cambia Password` per confermare la modifica.

.. _menù_principale_cambio_password:
.. figure:: _static/menù_principale_cambio_password.png
   :width: 14 cm
   :align: center

   Pagina di cambio password - Abilitazione

.. _menù_principale_cambio_password_2:
.. figure:: _static/menù_principale_cambio_password_2.png
   :width: 14 cm
   :align: center

   Pagina di cambio password - Inserimento nuova password

.. _menù_principale_cambio_password_3:
.. figure:: _static/menù_principale_cambio_password_3.png
   :width: 14 cm
   :align: center

   Pagina di cambio password - Conferma cambio password

ABILITAZIONE UTENTE
===================
Per abilitare la modifica dei parametri di lavoro inserire la password nella pagina in :numref:`menù_principale_login` e premere il pulsante :kbd:`Ok`.

Se la password è corretta l'icona |icona_menù_principale_unlocked| apparirà nella pagina del :guilabel:`Menù Principale`.

LOGOUT
======
Per disabilitare la modifica dei parametri di lavoro nella pagina in :numref:`menù_principale_login` premere il pulsante :kbd:`Logout`.

L'icona |icona_menù_principale_locked| apparirà nella pagina del :guilabel:`Menù Principale`.
