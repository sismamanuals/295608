#####
SCOPO
#####
Il presente Manuale di istruzioni del software è parte integrante del manuale d'uso e manutenzione del sistema su cui è installato. 

************
INTRODUZIONE
************
Le istruzioni, i disegni e la documentazione contenuti nel presente Manuale di Istruzioni sono di natura tecnica riservata, di stretta proprietà del Fabbricante e non possono essere riprodotti in alcun modo, né integralmente, né parzialmente.

L'acquirente ha, inoltre, la responsabilità di assicurarsi che, nel caso in cui il Fabbricante comunichi delle modifiche/miglioramenti al presente documento, solo le versioni aggiornate del Manuale siano effettivamente presenti nei punti di utilizzo.

.. DANGER::
   |danger| L'acquirente ha l'obbligo di eseguire una valutazione del rischio relativamente al sistema installato e di formare ed informare gli operatori riguardo ai contenuti del presente manuale, nonché sugli eventuali rischi residui derivanti dalla propria analisi.

*****************
REVISIONI MANUALE
*****************
.. _tabella_revisioni_manuale:

.. csv-table:: Tabella revisioni del manuale
   :header: "VERSIONE", "DATA"
   :widths: 25, 25
   :align: center

   "01", "05/2017"
   "02", "06/2020"

****************
MODELLI PRODOTTO
****************
Nella :numref:`tabella_codici_macchina` sono riportati i modelli di apparecchiatura ai quali si applicano le indicazioni riportate nel presente Manuale Software. Viene indicato solo l'indicazione del modello, ritenendo compresi tutte le possibili varianti ed accessori utilizzabili.

.. _tabella_codici_macchina:

.. csv-table:: Modelli di apparecchiatura
   :header: "MODELLO", "MODELLO"
   :widths: 30, 30
   :align: center

   "LM-D 180", "LM-D 210"
   "LM-D 180 OPEN", "LM-D 210 OPEN"
   "LM-D 150 READY", "LM-D 180 READY"
   "LM-D LYNX 180", "LM-D LYNX 210"
   "LM 250", ""

********************************************
LISTA DOCUMENTI CHE COMPONGONO LE ISTRUZIONI
********************************************
Il Manuale di Istruzioni dell'apparecchiatura è composto dall'insieme dei Manuali della :numref:`tabella_codici_manuali`. Per i Capitoli di descrizioni comuni (definizioni, ecc...) fare riferimento al Manuale di Istruzioni – Uso e Manutenzione.

.. _tabella_codici_manuali:

.. csv-table:: Elenco Manuali
   :header: "CODICE", "DESCRIZIONE"
   :widths: 30, 30
   :align: center

   "295606", "Uso e Manutenzione"
   "295622", "Movimentazione e Stoccaggio"
   "295608", "Uso Software"

.. _uso_del_software:

****************
USO DEL SOFTWARE
****************
Il software permette la gestione dei parametri di lavoro tramite *touch screen* o tramite pulsantiera presente nella camera di lavoro.

A titolo esemplificativo il software permette di:

* gestire ricette di lavorazione (:numref:`gestione_ricette`);
* accedere allo storico allarmi (:numref:`segnalazione_allarmi`);
* verificare lo stato macchina (:numref:`funzioni_speciali`);
* impostare i parametri di lavoro (:numref:`parametri_di_lavoro`);
* gestire un mandrino - *opzionale* (:numref:`uso_mandrino`).

Di seguito sono presentate e descritte tutte le funzionalità del software per poter controllare le prestazioni dell'apparecchiatura ed eseguire le lavorazioni desiderate.
