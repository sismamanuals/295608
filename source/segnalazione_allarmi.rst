.. _segnalazione_allarmi:

#######################
SEGNALAZIONI DI ALLARME
#######################
L'apparecchiatura fornisce una ampia gamma di segnalazioni di allarmi ed avvisi, in modo da permettere all'operatore di avere sempre la percezione immediata dello stato corrente del sistema.

Quando l'apparecchiatura deve segnalare un avviso o è in corso un allarme, inizia a lampeggiare l'icona relativa alla segnalazione allarmi (``J`` della :numref:`tabella_comandi`).

L'icona di colore grigio indica assenza di segnalazioni di allarmi, mentre l'icona gialla lampeggiante indica segnalazioni di allarmi.

Toccando il pulsante si passa alla visualizzazione della pagina :guilabel:`Allarmi`, contenente l'elenco delle segnalazioni attive.

Un maggior dettaglio sugli allarmi si può trovare nella sezione :guilabel:`Ultimi Allarmi` (:numref:`ultimi_allarmi`).
