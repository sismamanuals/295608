.. Comando di interruzione linea

.. |br| raw:: html

   <br>

.. Definizione delle immagini inline

.. |icona_mandrino| image:: _static/icona_mandrino.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_direzione_rotazione| image:: _static/icona_mandrino_direzione_rotazione.png
      :height: 2 cm
      :align: middle

.. |icona_mandrino_no_rotazione| image:: _static/icona_mandrino_no_rotazione.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_rotazione_oraria| image:: _static/icona_mandrino_rotazione_oraria.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_rotazione_antioraria| image:: _static/icona_mandrino_rotazione_antioraria.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_tempo_rotazione| image:: _static/icona_mandrino_tempo_rotazione.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_diminuzione_velocità| image:: _static/icona_down.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_aumento_velocità| image:: _static/icona_up.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_selezione_velocità| image:: _static/icona_mandrino_selezione_velocità.png
      :height: 1 cm
      :align: middle

.. |icona_mandrino_auto_manuale| image:: _static/icona_mandrino_auto_manuale.png
      :height: 2 cm
      :align: middle

.. |icona_mandrino_avvio| image:: _static/icona_mandrino_avvio.png
      :height: 1 cm
      :align: middle

.. _uso_mandrino:

################
USO DEL MANDRINO
################
.. NOTE::
   |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette.

Nel caso in cui l'apparecchiatura sia predisposta con questo accessorio, sarà visibile nel menù principale il pulsante |icona_mandrino| (:numref:`menù_mandrino`).

Premerlo per accedere alla pagina :guilabel:`Mandrino` (:numref:`menù_mandrino_opzioni`).

.. _menù_mandrino:
.. figure:: _static/menù_mandrino.png
   :width: 14 cm
   :align: center

   :guilabel:`Menù Principale` con abilitazione MANDRINO

.. _menù_mandrino_opzioni:
.. figure:: _static/menù_mandrino_opzioni.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Mandrino`

.. _tabella_comandi_mandrino:

.. csv-table:: Tabella comandi mandrino
   :header: "n°", "ICONA", "DESCRIZIONE"
   :widths: 10, 35, 55
   :align: center

   "A", "|icona_mandrino_direzione_rotazione|", "Selettore di impostazione della |br| direzione di rotazione"
   "B", "|icona_mandrino_no_rotazione|", "Spegnimento MANDRINO"
   "C", "|icona_mandrino_rotazione_oraria|", "Rotazione in senso orario"
   "D", "|icona_mandrino_rotazione_antioraria|", "Rotazione in senso antiorario"
   "E", "|icona_mandrino_tempo_rotazione|", "Tempo impiegato per una rotazione |br| completa (360°)"
   "F", "|icona_mandrino_diminuzione_velocità| |icona_mandrino_aumento_velocità|", "Pulsanti di modifica manuale della |br| velocità di rotazione"
   "G", "|icona_mandrino_selezione_velocità|", "Barra di selezione manuale della |br| velocità di rotazione"
   "H", "|icona_mandrino_auto_manuale|", "Pulsante di selezione modalità di funzionamento"
   "I", "|icona_mandrino_avvio|", "Pulsante di movimentazione mandrino senza |br| emissione di radiazione laser (test/posizionamento)"

*******************
MODALITÀ AUTOMATICA
*******************
Per utilizzare il mandrino in modalità automatica:

1. impostare la velocità desiderata tramite la barra di selezione (``G`` della :numref:`tabella_comandi_mandrino`) o i pulsanti di modifica di velocità (``F`` della :numref:`tabella_comandi_mandrino`). Sulla casella numerica (``E`` della :numref:`tabella_comandi_mandrino`) :kbd:`Tempo di rotazione` appare il tempo di rotazione impostato;
#. impostare il verso di rotazione desiderato tramite il selettore (``A`` della :numref:`tabella_comandi_mandrino`), spostandolo dalla posizione di riposo a quella relativa alla rotazione oraria o antioraria;
#. impostare il selettore (``H`` della :numref:`tabella_comandi_mandrino`) su :kbd:`Automatico`;
#. impostare l'estensione dell'angolo di saldatura in gradi [°];
#. impostare il laser per poter eseguire la saldatura con la ricetta desiderata;
#. premere il pedale per ottenere la rotazione del mandrino in sincronia con l'emissione degli impulsi laser.

****************
MODALITÀ MANUALE
****************
Per utilizzare il mandrino in modalità manuale:

1. impostare il selettore (``H`` della :numref:`tabella_comandi_mandrino`) su :kbd:`Manuale`;
#. impostare la velocità desiderata tramite la barra di selezione (``G`` della :numref:`tabella_comandi_mandrino`) o i pulsanti di modifica di velocità (``F`` della :numref:`tabella_comandi_mandrino`). Sulla casella numerica (``E`` della :numref:`tabella_comandi_mandrino`) :kbd:`Tempo di rotazione` appare il tempo di rotazione impostato;
#. impostare il verso di rotazione desiderato tramite il selettore (``A`` della :numref:`tabella_comandi_mandrino`), spostandolo dalla posizione di riposo a quella relativa alla rotazione oraria od antioraria;
#. impostare il laser per poter eseguire la saldatura con la ricetta desiderata;
#. premere il pedale per ottenere la rotazione del mandrino in sincronia con l'emissione degli impulsi laser.

La durata di lavorazione è controllata dalla pressione del pedale.

*********************
MENÙ PAGINA DI LAVORO
*********************
Se viene abilitato l'accessorio MANDRINO nella :guilabel:`Pagina di Lavoro` compare l'icona in :numref:`menù_mandrino_pagina_di_lavoro`.

.. _menù_mandrino_pagina_di_lavoro:
.. figure:: _static/menù_mandrino_pagina_di_lavoro.png
   :width: 14 cm
   :align: center

   :guilabel:`Pagina di Lavoro` con abilitazione MANDRINO

Premendo l'icona si accede direttamente alla pagina :guilabel:`Mandrino` (:numref:`menù_mandrino_opzioni`).
