.. _manutenzione:

############
MANUTENZIONE
############
Dalla :numref:`menù_stato_macchina_contatori` si apre la pagina :guilabel:`Manutenzione` (:numref:`menù_pagina_manutenzione`).

.. _menù_pagina_manutenzione:
.. figure:: _static/menù_pagina_manutenzione.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`Manutenzione`

.. DANGER::
   |danger| Il rinvio della manutenzione è responsabilità del cliente. Il Fabbricante non risponde per danni a cose e persone intervenuti a causa di mancata manutenzione.

**************
CAMBIO LAMPADA
**************
Al superamento del limite di funzionamento della lampada, da controllare a cura dell'operatore, come indicato nella :numref:`stato_macchina` è obbligatorio contattare il Servizio Clienti SISMA per la sostituzione della lampada. Il Tecnico Autorizzato dal Fabbricante provvederà ad azzerare il contatore di funzionamento dopo la sostituzione.

***********************
CAMBIO FILTRI E LIQUIDO
***********************
Allo scadere del tempo impostato o premendo sui pulsanti presenti nella :numref:`menù_pagina_manutenzione` viene visualizzata la relativa schermata a pannello :numref:`menù_avviso_manutenzione_2`.

Inserendo la password è possibile azzerare il contatore della richiesta di manutenzione.

Le operazioni di reset dei contatori di sono riservate solamente a Personale Tecnico Autorizzato dal Fabbricante.

.. _menù_avviso_manutenzione_2:
.. figure:: _static/menù_avviso_manutenzione_2.png
   :width: 14 cm
   :align: center

   Menù Manutenzione - Filtri e liquido

Se il contatore di manutenzione supera i limiti impostati e non sono state eseguite le operazioni previste la segnalazione verrà mostrata ad ogni nuovo avvio.

È possibile posticipare la manutenzione e continuare il lavoro premendo il pulsante :kbd:`Cambia in seguito`.

CIRCUITO DI RAFFREDDAMENTO
==========================
Dopo ogni svuotamento del circuito del liquido di raffreddamento è obbligatorio eseguire la procedura di nuovo riempimento.

Per attivare la procedura guidata di riempimento selezionare il check presente nella :numref:`menù_pagina_manutenzione`.
Al successivo avvio, l'apparecchiatura visualizzerà la procedura guidata per ricaricare il circuito dell'acqua.
