.. Comando di interruzione linea

.. |br| raw:: html

    <br>

.. Definizione delle immagini inline

.. |icona_impulso_rettangolare| image:: _static/icona_impulso_rettangolare.png
      :height: 1 cm
      :align: middle

.. |icona_impulso_rettangolare_rampa_salita| image:: _static/icona_impulso_rettangolare_rampa_salita.png
      :height: 1 cm
      :align: middle

.. |icona_impulso_rettangolare_rampa_discesa| image:: _static/icona_impulso_rettangolare_rampa_discesa.png
      :height: 1 cm
      :align: middle

.. |icona_impulso_rettangolare_rampa_salita_discesa| image:: _static/icona_impulso_rettangolare_rampa_salita_discesa.png
      :height: 1 cm
      :align: middle

.. |icona_impulso_rettangolare_ripetuto| image:: _static/icona_impulso_rettangolare_ripetuto.png
      :height: 1 cm
      :align: middle

.. |icona_impulso_rettangolare_bassa_potenza| image:: _static/icona_impulso_rettangolare_bassa_potenza.png
      :height: 1 cm
      :align: middle

.. |icona_impulso_customshape| image:: _static/icona_impulso_customshape.png
      :height: 1 cm
      :align: middle

.. Lista icone menù laser - icona_impulso_customshape

.. |icona_forma_custom| image:: _static/icona_forma_custom.png
      :height: 1 cm
      :align: middle

.. |icona_personalizzazione_customshape| image:: _static/icona_personalizzazione_customshape.png
      :height: 1 cm
      :align: middle

.. |icona_selezione_segmento_customshape| image:: _static/icona_selezione_segmento_customshape.png
      :height: 1 cm
      :align: middle

.. |icona_potenza_sezione_customshape| image:: _static/icona_potenza_sezione_customshape.png
      :height: 1 cm
      :align: middle

.. |icona_durata_sezione_customshape| image:: _static/icona_durata_sezione_customshape.png
      :height: 1 cm
      :align: middle

.. |icona_menù_principale_locked_2| image:: _static/icona_menù_principale_locked.png
      :height: 1 cm
      :align: middle

.. |icona_menù_principale_unlocked_2| image:: _static/icona_menù_principale_unlocked.png
      :height: 1 cm
      :align: middle

.. _parametri_di_lavoro:

################################
IMPOSTAZIONE PARAMETRI DI LAVORO
################################
****************************************
AUTO LIMITAZIONE DEI PARAMETRI DI LAVORO
****************************************
La funzione di limitazione parametri di lavoro è un controllo di primo livello predittivo, basata su calcoli eseguiti dal software. Ulteriori livelli di protezione agiscono in funzione dei parametri effettivamente misurati durante le lavorazioni.

Questa funzione, sempre attiva e non disattivabile, limita automaticamente i parametri in maniera da non superare in nessun caso i limiti massimi di funzionamento.

La funzione permette piena libertà di impostazione del parametro :kbd:`Potenza`, andando ad agire con una limitazione automatica dei parametri :kbd:`Frequenza` e :kbd:`Durata`.

Nel momento in cui la funzione interviene l'operatore viene informato tramite indicazione a video realizzata tramite una freccia verso il basso posta a fianco del valore limitato. Nell'esempio in :numref:`funzione_boost_pagina_di_lavoro_riduzione` si evidenzia la riduzione di frequenza.

.. _funzione_boost_pagina_di_lavoro_riduzione:
.. figure:: _static/funzione_boost_pagina_di_lavoro_riduzione.png
   :width: 14 cm
   :align: center

   Auto limitazione parametri di lavoro

.. _funzione_boost:

****************
FUNZIONE *BOOST*
****************
.. NOTE::
   |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette.

La funzione *BOOST* permette di superare, per un tempo limitato, il limite di potenza massima media dell'apparecchiatura.

Per abilitare la funzione:

* andare nella pagina di modifica del parametro :kbd:`Frequenza` o :kbd:`Durata`;
* abilitare il flag di *BOOST*.

.. _funzione_boost_abilitazione:
.. figure:: _static/funzione_boost_abilitazione.png
   :width: 14 cm
   :align: center

   Abilitazione funzione *BOOST*

Aumentando i parametri di :kbd:`Frequenza` o :kbd:`Durata` oltre il limite normalmente ammesso l'indicazione del parametro :kbd:`Frequenza` varia la sua colorazione diventando arancione.

.. _funzione_boost_on:
.. figure:: _static/funzione_boost_on.png
   :width: 14 cm
   :align: center

   Attivazione funzione *BOOST*

Nel caso la lavorazione dovesse superare il tempo massimo permesso il sistema interrompe la lavorazione e un avviso a video avverte l'operatore.

Per riprendere la lavorazione è obbligatorio rilasciare e poi ridare il comando di avvio.

Con la funzione *BOOST* attivata appare anche una ulteriore indicazione nella :guilabel:`Pagina di Lavoro` (:numref:`funzione_boost_pagina_di_lavoro`).

.. _funzione_boost_pagina_di_lavoro:
.. figure:: _static/funzione_boost_pagina_di_lavoro.png
   :width: 14 cm
   :align: center

   :guilabel:`Pagina di Lavoro` con funzione *BOOST* attivata

.. _funzione_impulso_laser:

*******************************
SELEZIONE TIPO DI IMPULSO LASER
*******************************
Il sistema genera, come impostazione predefinita, un impulso laser con forma rettangolare.

Sono disponibili anche altre forme di impulso già definite e utilizzabili, per migliorare il processo di saldatura, a seconda delle necessità.

Per complesse operazioni di saldatura è possibile anche l'utilizzo della funzione *Custom Shape* per formare una specifica e personalizzata forma di impulso.

Le forme di impulso preimpostate sono:

.. _tabella_impulsi_laser:

.. csv-table:: Impulsi laser
   :header: "", "ICONA", "DESCRIZIONE"
   :widths: 10, 20, 60
   :class: tabular
   :align: center

   "A", "|icona_impulso_rettangolare|", "**Modo impulso singolo rettangolare**"
   "B", "|icona_impulso_rettangolare_rampa_salita|", "**Modo rampa ascendente**: all'impulso singolo |br| rettangolare viene aggiunta una rampa iniziale."
   "C", "|icona_impulso_rettangolare_rampa_discesa|", "**Modo rampa discendente**: all'impulso singolo |br| rettangolare viene aggiunta una rampa finale."
   "D", "|icona_impulso_rettangolare_rampa_salita_discesa|", "**Modo trapezoidale**: all'impulso singolo vengono |br| aggiunte una rampa iniziale ed una finale."
   "E", "|icona_impulso_rettangolare_ripetuto|", "**Modo ripetuto**: vengono generati 3 impulsi rettangolari |br| uguali consecutivi spaziati da un intervallo temporale |br| di 1 ms."
   "F", "|icona_impulso_rettangolare_bassa_potenza|", "**Modo singolo impulso a scala espansa**: impulso |br| singolo con potenza dimezzata per avere più |br| risoluzione alle basse potenze."
   "G", "|icona_impulso_customshape|", "**Modo formatore impulso** o **Custom shape**: permette |br| di impostare fino a 10 sezioni differenti per |br| particolari esigenze di saldatura (vedere :numref:`impulso_customshape`)."

.. _impulso_customshape:

****************************
IMPULSO LASER *CUSTOM SHAPE*
****************************
Questa funzione permette l'ottimizzazione dei processi di saldatura su alcuni materiali, garantendo un corretto ciclo termico di riscaldamento, fusione e raffreddamento.

Quando viene scelta questa modalità di forma di impulso laser la :guilabel:`Pagina di Lavoro` viene aggiornata e al posto dell'indicatore della potenza e della durata di sparo viene visualizzato un grafico che descrive la forma *Custom Shape* dell'impulso.

.. _menù_impulso_customshape:
.. figure:: _static/menù_impulso_customshape.png
   :width: 14 cm
   :align: center

   Menù impulso *Custom shape*

.. _tabella_funzioni_laser_customshape:

.. csv-table:: Funzioni impulso laser *Custom Shape*
   :header: "", "ICONA", "DESCRIZIONE"
   :widths: 10, 25, 55
   :class: tabular
   :align: center

   "A", "|icona_forma_custom|", "Grafico che rappresenta la forma impulso |br| personalizzata dall'utente"
   "B", "|icona_impulso_customshape|", "Selezione dell'impulso laser *Custom Shape*"
   "C", "|icona_personalizzazione_customshape|", "Pulsante che permette di accedere alla pagina |br| di personalizzazione della forma |br| di impulso *Custom Shape*"

Per creare o modificare una forma di impulso basta toccare sul grafico visualizzato nella pagina di lavoro, o sull'apposito pulsante |icona_personalizzazione_customshape|.

Il *Custom Shape* è un impulso laser che può essere rappresentato come un unico impulso composto da 10 sezioni, ognuna delle quali personalizzabile per potenza e durata.

.. _menù_impulso_customshape_2:
.. figure:: _static/menù_impulso_customshape_2.png
   :width: 14 cm
   :align: center

   Menù impulso *Custom shape* - Schermata di impostazione

.. _tabella_funzioni_laser_customshape_2:

.. csv-table:: Funzioni impulso laser *Custom Shape* - Impostazione parametri
   :header: "", "ICONA", "DESCRIZIONE"
   :widths: 10, 25, 55
   :class: tabular
   :align: center

   "A", "|icona_selezione_segmento_customshape|", "Selezione della sezione di grafico"
   "B", "|icona_potenza_sezione_customshape|", "Selezione della potenza dell'impulso"
   "C", "|icona_durata_sezione_customshape|", "Selezione della durata dell'impulso"

Per ridurre o aumentare il numero della sezione corrente e i valori potenza e durata della sezione basta toccare sugli appositi pulsanti posti a sinistra e a destra di ogni valore.

La sezione corrente viene visualizzata con colore giallo all'interno del grafico.

Si può anche toccare direttamente sul valore che si desidera modificare. In questo caso viene visualizzato il tastierino numerico che permette di inserire il valore desiderato.

Il grafico visualizza in tempo reale la forma dell'impulso che si sta creando. Sotto il grafico vengono riportati i valori della :kbd:`Pmax` (potenza massima %), l'energia e la durata [ms] complessiva dell'intero impulso.

Premendo sul pulsante |icona_personalizzazione_customshape| si accede al menù grafico di modifica.

.. _menù_grafico_impulso_customshape:
.. figure:: _static/menù_grafico_impulso_customshape.png
   :width: 14 cm
   :align: center

   Menù grafico impulso *Custom shape*

Per modificare l'impulso:

* Abilitare le modifiche con il pulsante |icona_menù_principale_locked_2|;
* Trascinare all'interno dell'area del grafico per ridimensionare il segmento di cui il punto selezionato è uno dei vertici;
* Accettare e bloccare le modifiche con il pulsante |icona_menù_principale_unlocked_2| (l'icona tornerà |icona_menù_principale_locked_2|).

Il salvataggio della ricetta comprende anche il salvataggio dell'eventuale impulso *Custom Shape* definito dall'utente.
