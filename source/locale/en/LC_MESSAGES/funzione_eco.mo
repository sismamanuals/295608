��    !      $              ,  I   -  U   w  K   �  7     M   Q  )   �  -   �     �     �     �     �     �            m     �   �  5   K  5   �     �  �   �     w     �  B   �  c   �  �   D     �     �            �   +  �   �  �   Z	  �  �	  I   �  U   �  K   #  7   o  M   �  )   �  -        M     O     Q     S     U     a     n  _   s  �   �  ,   �  ,   �     �  �   �     �     �  3   �  Q   �  a   )     �     �     �     �  �   �  �   �  v      .. image:: _static/icona_eco_functions.png
   :alt: icona_eco_functions_2 .. image:: _static/icona_modo_eco_performance.png
   :alt: icona_modo_eco_performance .. image:: _static/icona_modo_eco_silent.png
   :alt: icona_modo_eco_silent .. image:: _static/icona_salva.png
   :alt: icona_salva .. image:: _static/icona_tempo_sleep_mode.png
   :alt: icona_tempo_sleep_mode .. image:: _static/menù_funzione_eco.png .. image:: _static/notice.png
   :alt: notice A B C D DESCRIZIONE FUNZIONE ECO ICONA Il pulsante :kbd:`Performance` seleziona la disponibilità costante delle massime prestazioni della macchina; Il software di gestione del dispositivo include alcune funzionalità di risparmio energia a cui si accede tramite il pulsante |icona_eco_functions_2| (``I`` della :numref:`tabella_comandi`). Impostare il funzionamento in |br| :kbd:`Performance` Impostare il funzionamento in |br| :kbd:`Silent Mode` Impulsi laser La casella :kbd:`ECO Sleep Mode` definisce il tempo di inattività della macchina dopo il quale si attiva la modalità di stand-by. Se impostato a 0 la modalità è disattivata. Pagina :guilabel:`ECO` Pulsante Salva Tempo di :kbd:`ECO Sleep Mode` per lo |br| Stand-by della macchina Una volta impostati i valori premere il pulsante |icona_salva| per rendere le modifiche definitive. il pulsante :kbd:`Silent Mode` seleziona la modalità di funzionamento silenziosa (con ridotta velocità delle ventole di raffreddamento). |icona_modo_eco_performance| |icona_modo_eco_silent| |icona_salva| |icona_tempo_sleep_mode| |notice| La modalità Stand-by riduce i consumi e, poiché vengono spente le parti più importanti e delicate del sistema, ne allunga il tempo di servizio. |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette. |notice| Nel caso sia presente la funzione *BOOST* (:numref:`funzione_boost`) la modalità di funzionamento silenziosa non sarà attivabile. Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 .. image:: _static/icona_eco_functions.png
   :alt: icona_eco_functions_2 .. image:: _static/icona_modo_eco_performance.png
   :alt: icona_modo_eco_performance .. image:: _static/icona_modo_eco_silent.png
   :alt: icona_modo_eco_silent .. image:: _static/icona_salva.png
   :alt: icona_salva .. image:: _static/icona_tempo_sleep_mode.png
   :alt: icona_tempo_sleep_mode .. image:: _static/menù_funzione_eco.png .. image:: _static/notice.png
   :alt: notice A B C D DESCRIPTION ECO FUNCTION ICON The :kbd:`Performance` button selects the constant availability of maximum machine performance; The device management software also includes some energy-saving features that are accessed through the |icona_eco_functions_2| button (``I`` of the :numref:`tabella_comandi`). Set the operation to |br| :kbd:`Performance` Set the operation to |br| :kbd:`Silent mode` Laser pulses The :kbd:`ECO Sleep Mode` box sets the time the machine is inactive for before the standby mode is activated. The mode is disabled if this is set to 0. :guilabel:`ECO` page Save button :kbd:`ECO Sleep Mode` time for |br| machine standby After setting the values, press the |icona_salva| button to finalise the changes. the :kbd:`Silent Mode` button selects the silent operating mode (slow speed of the cooling fans). |icona_modo_eco_performance| |icona_modo_eco_silent| |icona_salva| |icona_tempo_sleep_mode| |notice| The Standby mode decreases consumption and increases the service time of the system, since the most important and delicate parts of the system are turned off. |notice| The functions are enabled according to the equipment setup. If functions are absent, it means the system does not allow them. |notice| If the *BOOST* function is present (:numref:`funzione_boost`), the silent operating mode cannot be activated. 