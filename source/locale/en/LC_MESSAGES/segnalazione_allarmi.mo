Þ          T                  ¾         L  ´   Ó             w   $         8  k   Ó     ?     Ó  l   á  j   N   L'apparecchiatura fornisce una ampia gamma di segnalazioni di allarmi ed avvisi, in modo da permettere all'operatore di avere sempre la percezione immediata dello stato corrente del sistema. L'icona di colore grigio indica assenza di segnalazioni di allarmi, mentre l'icona gialla lampeggiante indica segnalazioni di allarmi. Quando l'apparecchiatura deve segnalare un avviso o Ã¨ in corso un allarme, inizia a lampeggiare l'icona relativa alla segnalazione allarmi (``J`` della :numref:`tabella_comandi`). SEGNALAZIONI DI ALLARME Toccando il pulsante si passa alla visualizzazione della pagina :guilabel:`Allarmi`, contenente l'elenco delle segnalazioni attive. Un maggior dettaglio sugli allarmi si puÃ² trovare nella sezione :guilabel:`Ultimi Allarmi` (:numref:`ultimi_allarmi`). Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 The equipment provides a broad range of alarm signals and alerts to enable the operator to always be immediately aware of the current state of the system. The gray icon means there is no alarm signal, while the yellow flashing icon means there are alarm signals. When the equipment has to give an alert or an alarm is in progress, the alarm signal icon starts to flash (``J`` of the :numref:`tabella_comandi`). ALARM SIGNALS Tap the button to switch to the display of the :guilabel:`Alarms` page, containing a list of active signals. More details about the alarms are found in the :guilabel:`Last Alarms` section (:numref:`ultimi_allarmi`). 