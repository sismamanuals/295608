��          \               �   (   �   U   �           *  �   <  /     9   <  �  v  (     U   ;     �     �  �   �  '   g  +   �   .. image:: _static/gestione_luci_LED.png .. image:: _static/icona_intensità_luminosa.png
   :alt: icona_intensità_luminosa_2 GESTIONE LUCI Gestione luci LED Toccando sul *touch screen* in corrispondenza dell'icona |icona_intensità_luminosa_2| nella :guilabel:`Pagina di Lavoro` (``F`` della :numref:`tabella_comandi`) si apre la finestra dedicata che permette di: regolare l'intensità luminosa dei faretti LED; spegnere o accendere la barra LED della camera di lavoro. Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 .. image:: _static/gestione_luci_LED.png .. image:: _static/icona_intensità_luminosa.png
   :alt: icona_intensità_luminosa_2 LIGHT MANAGEMENT LED light management Tap the *touch screen* on the |icona_intensità_luminosa_2| icon in the :guilabel:`Work page` (``F`` of the :numref:`tabella_comandi`) to open the relevant window that allows: adjusting the LED spotlight brightness; turning on or off the work chamber LED bar. 