��          t               �   /   �   -   �   -   +  /   Y  6  �     �  l   �  c   9  R   �     �  �  p  /     -   <  -   j  /   �  �   �     �  [   �  W   #  U   {  ^   �   .. image:: _static/caution.png
   :alt: caution .. image:: _static/danger.png
   :alt: danger .. image:: _static/notice.png
   :alt: notice .. image:: _static/warning.png
   :alt: warning Le descrizioni precedute da un pittogramma di colore rosso/giallo/arancione/azzurro contengono informazioni/prescrizioni molto importanti, anche per quanto riguarda la sicurezza, sia dell'operatore che dell'apparecchiatura. Di seguito si riportano le diciture con spiegazione di dettaglio del loro significato. PITTOGRAMMI |caution| Indica una situazione di rischio potenziale che, se non evitata, può causare morte o danno grave. |danger| Indica una situazione di rischio imminente che, se non evitata, causa morte o danno grave. |notice| Viene utilizzato per affrontare le pratiche non legate a lesioni fisiche. |warning| Indica una situazione di rischio potenziale che, se non prevista, potrebbe causare danni di minore o modesta entità. Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 .. image:: _static/caution.png
   :alt: caution .. image:: _static/danger.png
   :alt: danger .. image:: _static/notice.png
   :alt: notice .. image:: _static/warning.png
   :alt: warning Descriptions preceded by a red/yellow/orange/blue symbol contain very important information/instructions which involve the safety both of the operator and of the equipment. The following is a detailed explanation of the symbols and their meaning. SYMBOLS |caution| This is a potential risk that, if not prevented, may cause death or major injury. |danger| This is an imminent risk that, if not prevented, causes death or major injury. |notice| This is used when discussing practices that do not involve potential injury. |warning| This is a potential risk that, if not prevented, may cause minor or moderate injury. 