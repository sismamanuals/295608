��    n      �              �  ?   �  5   =  G   s  I   �  G     M   M  G   �  ;   �  W   	  K   w	  S   �	  =   
  M   U
  X   �
  I   �
  I   F  G   �  ;   �  -     -   B  3   p     �     �  	   �     �  	   �  	   �     �  �   �     �     �     �     �     �     �     �          *     9     ;     =     W     g     �     �     �  R   �     �     �  �   �  7   �     �     �  #   �               !    #  �   @  �   �     f     h     ~     �     �     �     �     �     �  }   �  �   S  B     )   [  ;   �  >   �  *      *   +  +   V     �     �     �  �   �  `   �  �   
  ~   �     !     6  
   >     I     M     V     h     u     �     �     �     �     �               1     I     e     v     �     �     �     �  �   �  �  ~  ?     5   Z  G   �  I   �  G   "  M   j  G   �  ;      W   <  K   �  S   �  =   4  M   r  X   �  I      I   c   G   �   ;   �   -   1!  -   _!  3   �!     �!     �!  	   �!     �!  	   �!  	   "     "  �   "     �"     �"     �"     �"     �"     �"     �"     �"  
   #     #     #     #     '#     3#     H#     J#     L#  X   N#     �#     �#  x   �#  (   5$     ^$     u$     �$     �$     �$     �$  �   �$  p   �%  �   &     �&     �&     �&     �&     �&  	   �&     �&  	   �&     �&  m   �&  �   ]'  +   �'     (  '   4(  .   \(     �(     �(     �(     �(     �(     )  �   )  N   �)  �   &*  V   �*     +  	   (+  
   2+     =+     A+     H+     Z+     g+     �+     �+     �+     �+     �+     �+     ,     #,     ;,     W,     h,     �,     �,     �,     �,  �   �,   .. image:: _static/icona_7010-W004.png
   :alt: icona_7010-W004 .. image:: _static/icona_back.png
   :alt: icona_back .. image:: _static/icona_diametro_spot.png
   :alt: icona_diametro_spot .. image:: _static/icona_durata_impulso.png
   :alt: icona_durata_impulso .. image:: _static/icona_eco_functions.png
   :alt: icona_eco_functions .. image:: _static/icona_finestra_allarmi.png
   :alt: icona_finestra_allarmi .. image:: _static/icona_forma_impulso.png
   :alt: icona_forma_impulso .. image:: _static/icona_forward.png
   :alt: icona_forward .. image:: _static/icona_frequenza_ripetizione.png
   :alt: icona_frequenza_ripetizione .. image:: _static/icona_grafico_energia.png
   :alt: icona_grafico_energia .. image:: _static/icona_intensità_luminosa.png
   :alt: icona_intensità_luminosa .. image:: _static/icona_manopola.png
   :alt: icona_manopola .. image:: _static/icona_menù_principale.png
   :alt: icona_menù_principale .. image:: _static/icona_menù_principale_ricette.png
   :alt: icona_descrizione_ricetta .. image:: _static/icona_on_off_lampada.png
   :alt: icona_on_off_lampada .. image:: _static/icona_on_off_shutter.png
   :alt: icona_on_off_shutter .. image:: _static/icona_pagina_lavoro.png
   :alt: icona_pagina_lavoro .. image:: _static/icona_potenza.png
   :alt: icona_potenza .. image:: _static/menù_pagina_di_lavoro.png .. image:: _static/notice.png
   :alt: notice .. image:: _static/pulsantiera_camera_di_lavoro.png :guilabel:`Pagina di Lavoro` :kbd:`%` :kbd:`Hz` :kbd:`LIGHT` :kbd:`ms` :kbd:`Ø` A All'interno della :guilabel:`Pagina di Lavoro` si trovano le funzioni ed i pulsanti attraverso i quali l'utilizzatore sceglie il tipo di lavorazione e le impostazioni dei parametri di funzionamento del sistema. Altre icone B C D DESCRIZIONE Descrizione ricetta Diametro dello spot laser Diametro spot laser Durata impulso E F FUNZIONI PAGINA DI LAVORO Forma d'impulso Frequenza di ripetizione G H I I primi 4 tasti, premuti singolarmente, permettono di selezionare rispettivamente: ICONA Icone pagina di lavoro Il pulsante :kbd:`LIGHT` permette di selezionare il controllo  per la regolazione dell'intensità luminosa dei faretti LED presenti in camera di lavoro. Indicatore grafico Energia e Potenza Media dell'impulso Indicatore shutter aperto Intensità luminosa LED Intensità luminosa dei faretti LED J K L La pressione del pulsante :kbd:`%` e di uno qualunque dei restanti (tranne il pulsante :kbd:`ms`) blocca la modifica dei valori impostati. Il blocco viene segnalato con l'accensione del led verde relativo ai pulsanti premuti. Per disattivare il blocco è sufficiente premere un tasto. La selezione di un parametro viene segnalata con l'accensione di un LED verde in corrispondenza del tasto e da una segnalazione a schermo. La spia di colore rosso offre una segnalazione di *shutter* aperto, indicando la condizione in cui il sistema è pronto all'emissione di radiazione laser. M Manopola di selezione N O P PAGINA DI LAVORO PULSANTIERA IN CAMERA DI LAVORO Potenza Impostata Potenza media Premendo contemporaneamente i primi 2 pulsanti è permesso il cambio *ricette*, rimanendo nella :guilabel:`Pagina di Lavoro`. Premendo la manopola si varia da un parametro ad un altro tra quelli presenti nella :guilabel:`Pagina di Lavoro`. La manopola è attiva solamente se ci si trova nella :guilabel:`Pagina di Lavoro`. Pulsante accensione e spegnimento lampada laser [#variante_ready]_ Pulsante accensione e spegnimento shutter Pulsante per accedere alla pagina :guilabel:`ECO Functions` Pulsante per accedere alla pagina :guilabel:`Menù principale` Pulsante per accedere alla pagina seguente Pulsante per tornare alla pagina di lavoro Pulsante per tornare alla pagina precedente Pulsantiera di comando Pulsantiera in camera di lavoro SIMBOLO Se si sta visualizzando la :guilabel:`Pagina di Lavoro` (:numref:`menù_pagina_di_lavoro`) e la *lampada laser* è accesa, la doppia pressione in rapida sequenza del pulsante :kbd:`LIGHT` permette di aprire e chiudere lo *shutter*. Segnalazione stato allarmi attivi e pulsante |br| per accedere alla finestra :guilabel:`Allarmi` Tramite la pulsantiera presente in camera di lavoro è possibile interagire con il sistema e modificare le impostazioni dei parametri di funzionamento. Tramite la rotazione della manopola si può modificare il valore del parametro selezionato, incrementandolo o decrementandolo. diametro dello spot. durata; frequenza; n° potenza; |icona_7010-W004| |icona_back| |icona_descrizione_ricetta| |icona_diametro_spot| |icona_durata_impulso| |icona_eco_functions| |icona_finestra_allarmi| |icona_forma_impulso| |icona_forward| |icona_frequenza_ripetizione| |icona_grafico_energia| |icona_intensità_luminosa| |icona_manopola| |icona_menù_principale| |icona_on_off_lampada| |icona_on_off_shutter| |icona_pagina_lavoro| |icona_potenza| |notice| Per i modelli *READY* l'accensione della lampada attiva automaticamente, dopo alcuni secondi, anche la possibilità di attivare l'impulso laser.. Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 .. image:: _static/icona_7010-W004.png
   :alt: icona_7010-W004 .. image:: _static/icona_back.png
   :alt: icona_back .. image:: _static/icona_diametro_spot.png
   :alt: icona_diametro_spot .. image:: _static/icona_durata_impulso.png
   :alt: icona_durata_impulso .. image:: _static/icona_eco_functions.png
   :alt: icona_eco_functions .. image:: _static/icona_finestra_allarmi.png
   :alt: icona_finestra_allarmi .. image:: _static/icona_forma_impulso.png
   :alt: icona_forma_impulso .. image:: _static/icona_forward.png
   :alt: icona_forward .. image:: _static/icona_frequenza_ripetizione.png
   :alt: icona_frequenza_ripetizione .. image:: _static/icona_grafico_energia.png
   :alt: icona_grafico_energia .. image:: _static/icona_intensità_luminosa.png
   :alt: icona_intensità_luminosa .. image:: _static/icona_manopola.png
   :alt: icona_manopola .. image:: _static/icona_menù_principale.png
   :alt: icona_menù_principale .. image:: _static/icona_menù_principale_ricette.png
   :alt: icona_descrizione_ricetta .. image:: _static/icona_on_off_lampada.png
   :alt: icona_on_off_lampada .. image:: _static/icona_on_off_shutter.png
   :alt: icona_on_off_shutter .. image:: _static/icona_pagina_lavoro.png
   :alt: icona_pagina_lavoro .. image:: _static/icona_potenza.png
   :alt: icona_potenza .. image:: _static/menù_pagina_di_lavoro.png .. image:: _static/notice.png
   :alt: notice .. image:: _static/pulsantiera_camera_di_lavoro.png :guilabel:`Work Page` :kbd:`%` :kbd:`Hz` :kbd:`LIGHT` :kbd:`ms` :kbd:`Ø` A The :guilabel:`Work Page` contains the functions and buttons used by the user to select the type of work and the settings of the system operating parameters. Other icons B C D DESCRIPTION Recipe description Laser spot diameter Laser spot diameter Pulse time E F WORK PAGE FUNCTIONS Pulse shape Repetition frequency G H I The first 4 keys, when pressed individually, allow respectively selecting the following: ICON Work page icons The :kbd:`LIGHT` button selects the control for adjusting the light intensity of the LED spotlights in the work chamber. Pulse Energy and Average Power indicator Open shutter indicator LED brightness intensity LED spotlight brightness J K L Pressing the :kbd:`%` button and any of the remaining buttons (except the :kbd:`ms` button) locks editing the set values. This lock is shown by the green LED turning on for the buttons which have been pressed. Press any key to deactivate the lock. The selection of a parameter is signalled by a green LED turned on near the key and an indication on the screen. The red light indicates when the *shutter* is open, showing the condition when the system is ready for laser radiation emission. M Selection knob N O P WORK PAGE WORK CHAMBER PUSH BUTTON PANEL Set power Average power Press the first 2 buttons at the same time to change *recipes*, while remaining in the :guilabel:`Work Page`. Press the knob to toggle between parameters present in the :guilabel:`Work Page`. The knob is active only when you are in the :guilabel:`Work Page`. Laser lamp on/off button [#variante_ready]_ Shutter on/off button :guilabel:`ECO Functions` access button Button to go to the :guilabel:`Main Menu` page Go to next page button Return to work page button Return to previous page button Control push button panel Work chamber push button panel SYMBOL If the :guilabel:`Work Page` is displayed (:numref:`menù_pagina_di_lavoro`) and the *laser lamp* is on, pressing the :kbd:`LIGHT` twice rapidly allows opening and closing the *shutter*. Active alarm status indicator |br| and :guilabel:`Alarms` window access button The push button panel in the work chamber allows you to interact with the system and change the operating parameter settings shown on the operator display. By turning the knob, you can increase or decrease the value of the selected parameter. spot diameter. duration; frequency; no. power; |icona_7010-W004| |icona_back| |icona_descrizione_ricetta| |icona_diametro_spot| |icona_durata_impulso| |icona_eco_functions| |icona_finestra_allarmi| |icona_forma_impulso| |icona_forward| |icona_frequenza_ripetizione| |icona_grafico_energia| |icona_intensità_luminosa| |icona_manopola| |icona_menù_principale| |icona_on_off_lampada| |icona_on_off_shutter| |icona_pagina_lavoro| |icona_potenza| |notice| For *READY* models, turning on the lamp automatically activates also the possibility of activating the laser pulse after a few seconds. 