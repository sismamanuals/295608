��          T               �   �   �   �   L  �   �     �  �   �  w   $  �  �  �   7  �   &  �   �       }   �  �      L'apparecchiatura fornisce una ampia gamma di segnalazioni di allarmi ed avvisi, in modo da permettere all'operatore di avere sempre la percezione immediata dello stato corrente del sistema. L'icona di colore grigio indica assenza di segnalazioni di allarmi, mentre l'icona gialla lampeggiante indica segnalazioni di allarmi. Quando l'apparecchiatura deve segnalare un avviso o è in corso un allarme, inizia a lampeggiare l'icona relativa alla segnalazione allarmi (``J`` della :numref:`tabella_comandi`). SEGNALAZIONI DI ALLARME Toccando il pulsante si passa alla visualizzazione della pagina :guilabel:`Allarmi`, contenente l'elenco delle segnalazioni attive. Un maggior dettaglio sugli allarmi si può trovare nella sezione :guilabel:`Ultimi Allarmi` (:numref:`ultimi_allarmi`). Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 L’appareil fournit une vaste gamme d’indications d’alarmes et de mises en garde de manière à permettre à l'opérateur de travailler en toute sécurité et d'avoir toujours une perception immédiate de l'état courant du système. L’icône de couleur grise indique l’absence d’indications d’alarmes, tandis que l’icône jaune clignotante indique des indications d’alarmes. Lorsque l’appareil doit signaler un avertissement ou qu'une alarme est en cours, l'icône relative à l’indication des alarmes commence à clignoter (``J`` du :numref:`tabella_comandi`). INDICATIONS D'ALARME En touchant le bouton, on passe à l’affichage de la page :guilabel:`Alarmes` contenant la liste des indications activées. Des détails supplémentaires concernant les alarmes peuvent être consultés dans la section :guilabel:`Dernières alarmes` (:numref:`ultimi_allarmi`). 