��          t               �   /   �   -   �   -   +  /   Y  6  �     �  l   �  c   9  R   �     �  �  p  /     -   ;  -   i  /   �  D  �       �     ~   �  \   	  �   |	   .. image:: _static/caution.png
   :alt: caution .. image:: _static/danger.png
   :alt: danger .. image:: _static/notice.png
   :alt: notice .. image:: _static/warning.png
   :alt: warning Le descrizioni precedute da un pittogramma di colore rosso/giallo/arancione/azzurro contengono informazioni/prescrizioni molto importanti, anche per quanto riguarda la sicurezza, sia dell'operatore che dell'apparecchiatura. Di seguito si riportano le diciture con spiegazione di dettaglio del loro significato. PITTOGRAMMI |caution| Indica una situazione di rischio potenziale che, se non evitata, può causare morte o danno grave. |danger| Indica una situazione di rischio imminente che, se non evitata, causa morte o danno grave. |notice| Viene utilizzato per affrontare le pratiche non legate a lesioni fisiche. |warning| Indica una situazione di rischio potenziale che, se non prevista, potrebbe causare danni di minore o modesta entità. Project-Id-Version: LM-D Software Manuale Utente 295608 rev.02
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-25 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 .. image:: _static/caution.png
   :alt: caution .. image:: _static/danger.png
   :alt: danger .. image:: _static/notice.png
   :alt: notice .. image:: _static/warning.png
   :alt: warning Les descriptions précédées par un pictogramme de couleur rouge/jaune/orange/bleue contiennent des informations/prescriptions très importantes, y compris en ce qui concerne tant la sécurité de l’opérateur que de l’appareil. Ci-après figurent les indications avec l’explication détaillée de leur signification. PICTOGRAMMES |caution| Il indique une situation de risque potentiel qui, si elle n'est pas évitée, peut provoquer la mort ou des dégâts graves. |danger| Il indique une situation de risque imminent qui, si elle n'est pas évitée, provoque la mort ou des dégâts graves. |notice| Il est utilisé pour affronter les pratiques non liées à des blessures physiques. |warning| Il indique une situation de risque potentiel qui, si elle n'est pas prévue, pourrait provoquer des dégâts d’importance mineure ou modeste. 