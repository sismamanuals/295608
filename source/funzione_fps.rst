.. Definizione delle immagini inline

.. |icona_menù_funzione_FPS| image:: _static/icona_menù_funzione_FPS.png
      :height: 1 cm
      :align: middle

.. _funzione_FPS:

#########################################
FUNZIONE *FPS* (First Pulses Suppression)
#########################################
.. NOTE::
   |notice| Le funzioni vengono abilitate secondo le predisposizioni dell'apparecchiatura. La loro mancanza significa che il sistema non le permette.

Questa funzione migliora la regolarità dei primi impulsi che, normalmente, risultano più energetici rispetto ai successivi. Tale comportamento, dovuto alla stabilizzazione termica dei componenti ottici, avviene nei primi secondi di funzionamento.

La funzione *FPS* (First Pulses Suppression) serve per abilitare gli impulsi del laser mantenendo lo shutter chiuso, quindi senza emissione di radiazione all'esterno.

È attivabile dalla :guilabel:`Pagina di Lavoro` tramite il pulsante |icona_menù_funzione_FPS|.

* Il pulsante con testo giallo significa che la funzione *FPS* è abilitata;
* il pulsante con testo bianco significa che la funzione *FPS* è disabilitata.

Il numero sotto la scritta *FPS* indica i numeri di impulsi effettuati in questa modalità.

Ogni volta che viene abilitata o disabilitata la funzione *FPS* la lampada laser viene automaticamente spenta. Per riprendere a lavorare riabilitarla nuovamente (``L`` della :numref:`tabella_comandi`).

Riducendo a zero la frequenza di lavoro, la funzione *FPS* non è disponibile.

.. _menù_funzione_FPS:
.. figure:: _static/menù_funzione_FPS.png
   :width: 14 cm
   :align: center

   Pagina :guilabel:`FPS` (First Pulses Suppression)

**********************
FUNZIONE *FPS* MANUALE
**********************
Nel pulsante della funzione *FPS* (:numref:`menù_funzione_FPS`) viene visualizzato un numero che indica il numero di impulsi impostati in modalità *FPS* (ovvero senza emettere radiazione).

Per variare tale valore, toccare il *touch screen* in corrispondenza di :kbd:`Impulsi`. Impostando il numero degli impulsi *FPS* a zero si abilita la gestione manuale del numero di impulsi in modalità *FPS*. La gestione avviene tramite il pedale di abilitazione impulso:

* al primo scatto viene abilitata l'elettrovalvola di controllo del gas tecnico ed il sistema inizia a generare impulsi con shutter chiuso (quindi senza emettere radiazione all'esterno), con frequenza e potenza impostate nel programma in uso;
* premendo il pedale fino in fondo (secondo scatto) si apre lo shutter e si inizia la lavorazione.

.. _menù_funzione_FPS_auto:
.. figure:: _static/menù_funzione_FPS_auto.png
   :width: 14 cm
   :align: center

   Menù *FPS* Automatico/Manuale

*************************
FUNZIONE *FPS* AUTOMATICA
*************************
La modalità di gestione automatica della funzione *FPS* prevede di far eseguire al sistema un numero preimpostato di impulsi prima di emettere all'esterno la radiazione laser.

Per utilizzare questa modalità:

* toccare lo schermo in corrispondenza del pulsante *FPS*;
* abilitare la funzione *FPS*;
* impostare il numero di impulsi desiderato;
* chiudere la schermata.

Con un numero di impulsi *FPS* diverso da 0:

* al primo scatto del pedale viene abilitata solamente l'erogazione del gas tecnico;
* premendo il pedale fino in fondo (secondo scatto) si abilità la funzione *FPS* che eseguirà un numero di impulsi pari a quello impostato prima di emettere radiazione laser all'esterno della sorgente.

Ad ogni interruzione del comando pedale la funzione *FPS* riprenderà a contare il numero di impulsi laser dall'inizio.
