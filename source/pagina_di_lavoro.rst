.. Comando di interruzione linea

.. |br| raw:: html

   <br>

.. Definizione delle immagini inline

.. |icona_7010-W004| image:: _static/icona_7010-W004.png
      :height: 1 cm
      :align: middle

.. |icona_manopola| image:: _static/icona_manopola.png
      :height: 1 cm
      :align: middle

.. Lista icone menù principale

.. |icona_potenza| image:: _static/icona_potenza.png
      :height: 1 cm
      :align: middle

.. |icona_durata_impulso| image:: _static/icona_durata_impulso.png
      :height: 1 cm
      :align: middle

.. |icona_frequenza_ripetizione| image:: _static/icona_frequenza_ripetizione.png
      :height: 1 cm
      :align: middle

.. |icona_diametro_spot| image:: _static/icona_diametro_spot.png
      :height: 1 cm
      :align: middle

.. |icona_descrizione_ricetta| image:: _static/icona_menù_principale_ricette.png
      :height: 1 cm
      :align: middle

.. |icona_intensità_luminosa| image:: _static/icona_intensità_luminosa.png
      :height: 1 cm
      :align: middle

.. |icona_grafico_energia| image:: _static/icona_grafico_energia.png
      :height: 1 cm
      :align: middle

.. |icona_menù_principale| image:: _static/icona_menù_principale.png
      :height: 1 cm
      :align: middle

.. |icona_on_off_lampada| image:: _static/icona_on_off_lampada.png
      :height: 1 cm
      :align: middle

.. |icona_on_off_shutter| image:: _static/icona_on_off_shutter.png
      :height: 1 cm
      :align: middle

.. |icona_finestra_allarmi| image:: _static/icona_finestra_allarmi.png
      :height: 1 cm
      :align: middle

.. |icona_forma_impulso| image:: _static/icona_forma_impulso.png
      :height: 1 cm
      :align: middle

.. |icona_eco_functions| image:: _static/icona_eco_functions.png
      :height: 1 cm
      :align: middle

.. |icona_barra_comandi| image:: _static/icona_barra_comandi.png
      :height: 1 cm
      :align: middle

.. |icona_pagina_lavoro| image:: _static/icona_pagina_lavoro.png
      :height: 1 cm
      :align: middle

.. |icona_back| image:: _static/icona_back.png
      :height: 1 cm
      :align: middle

.. |icona_forward| image:: _static/icona_forward.png
      :height: 1 cm
      :align: middle

.. _pagina_di_lavoro:

################
PAGINA DI LAVORO
################
All'interno della :guilabel:`Pagina di Lavoro` si trovano le funzioni ed i pulsanti attraverso i quali l'utilizzatore sceglie il tipo di lavorazione e le impostazioni dei parametri di funzionamento del sistema.

.. _menù_pagina_di_lavoro:
.. figure:: _static/menù_pagina_di_lavoro.png
   :width: 14 cm
   :align: center

   :guilabel:`Pagina di Lavoro`

*************************
FUNZIONI PAGINA DI LAVORO
*************************

.. _tabella_comandi:

.. csv-table:: Icone pagina di lavoro
   :header: "n°", "ICONA", "DESCRIZIONE"
   :widths: 10, 20, 60
   :class: tabular
   :align: center

   "A", "|icona_potenza|", "Potenza Impostata"
   "B", "|icona_durata_impulso|", "Durata impulso"
   "C", "|icona_frequenza_ripetizione|", "Frequenza di ripetizione"
   "D", "|icona_diametro_spot|", "Diametro dello spot laser"
   "E", "|icona_descrizione_ricetta|", "Descrizione ricetta"
   "F", "|icona_intensità_luminosa|", "Intensità luminosa dei faretti LED"
   "G", "|icona_grafico_energia|", "Indicatore grafico Energia e Potenza Media dell'impulso"
   "H", "|icona_forma_impulso|", "Forma d'impulso"
   "I", "|icona_eco_functions|", "Pulsante per accedere alla pagina :guilabel:`ECO Functions`"
   "J", "|icona_finestra_allarmi|", "Segnalazione stato allarmi attivi e pulsante |br| per accedere alla finestra :guilabel:`Allarmi`"
   "K", "|icona_on_off_shutter|", "Pulsante accensione e spegnimento shutter"
   "L", "|icona_on_off_lampada|", "Pulsante accensione e spegnimento lampada laser [#variante_ready]_"
   "M", "|icona_menù_principale|", "Pulsante per accedere alla pagina :guilabel:`Menù principale`"

.. [#variante_ready] .. NOTE:: |notice| Per i modelli *READY* l'accensione della lampada attiva automaticamente, dopo alcuni secondi, anche la possibilità di attivare l'impulso laser..

.. _tabella_comandi_2:

.. csv-table:: Altre icone
   :header: "n°", "ICONA", "DESCRIZIONE"
   :widths: 10, 20, 60
   :class: tabular
   :align: center

   "N", "|icona_pagina_lavoro|", "Pulsante per tornare alla pagina di lavoro"
   "O", "|icona_back|", "Pulsante per tornare alla pagina precedente"
   "P", "|icona_forward|", "Pulsante per accedere alla pagina seguente"

*******************************
PULSANTIERA IN CAMERA DI LAVORO
*******************************
Tramite la pulsantiera presente in camera di lavoro è possibile interagire con il sistema e modificare le impostazioni dei parametri di funzionamento.

I primi 4 tasti, premuti singolarmente, permettono di selezionare rispettivamente:

* potenza;
* durata;
* frequenza;
* diametro dello spot.

.. _pulsantiera_camera_di_lavoro:
.. figure:: _static/pulsantiera_camera_di_lavoro.png
   :width: 14 cm
   :align: center

   Pulsantiera in camera di lavoro

.. csv-table:: Pulsantiera di comando
   :header: "SIMBOLO", "DESCRIZIONE"
   :widths: 25, 45
   :class: tabular
   :align: center

   "|icona_manopola|", "Manopola di selezione"
   ":kbd:`%`", "Potenza media"
   ":kbd:`ms`", "Durata impulso"
   ":kbd:`Hz`", "Frequenza di ripetizione"
   ":kbd:`Ø`", "Diametro spot laser"
   ":kbd:`LIGHT`", "Intensità luminosa LED"
   "|icona_7010-W004|", "Indicatore shutter aperto"

Il pulsante :kbd:`LIGHT` permette di selezionare il controllo  per la regolazione dell'intensità luminosa dei faretti LED presenti in camera di lavoro.

La selezione di un parametro viene segnalata con l'accensione di un LED verde in corrispondenza del tasto e da una segnalazione a schermo.

Premendo contemporaneamente i primi 2 pulsanti è permesso il cambio *ricette*, rimanendo nella :guilabel:`Pagina di Lavoro`.

Tramite la rotazione della manopola si può modificare il valore del parametro selezionato, incrementandolo o decrementandolo.

Premendo la manopola si varia da un parametro ad un altro tra quelli presenti nella :guilabel:`Pagina di Lavoro`. La manopola è attiva solamente se ci si trova nella :guilabel:`Pagina di Lavoro`.

La pressione del pulsante :kbd:`%` e di uno qualunque dei restanti (tranne il pulsante :kbd:`ms`) blocca la modifica dei valori impostati. Il blocco viene segnalato con l'accensione del led verde relativo ai pulsanti premuti. Per disattivare il blocco è sufficiente premere un tasto.

La spia di colore rosso offre una segnalazione di *shutter* aperto, indicando la condizione in cui il sistema è pronto all'emissione di radiazione laser.

Se si sta visualizzando la :guilabel:`Pagina di Lavoro` (:numref:`menù_pagina_di_lavoro`) e la *lampada laser* è accesa, la doppia pressione in rapida sequenza del pulsante :kbd:`LIGHT` permette di aprire e chiudere lo *shutter*.
