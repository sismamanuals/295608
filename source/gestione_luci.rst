.. Definizione delle immagini inline

.. |icona_intensità_luminosa_2| image:: _static/icona_intensità_luminosa.png
      :height: 1 cm
      :align: middle

.. _funzione_gestione_luci:

*************
GESTIONE LUCI
*************
Toccando sul *touch screen* in corrispondenza dell'icona |icona_intensità_luminosa_2| nella :guilabel:`Pagina di Lavoro` (``F`` della :numref:`tabella_comandi`) si apre la finestra dedicata che permette di:

* regolare l'intensità luminosa dei faretti LED;
* spegnere o accendere la barra LED della camera di lavoro.

.. _gestione_luci_LED:
.. figure:: _static/gestione_luci_LED.png
   :width: 14 cm
   :align: center

   Gestione luci LED
